#!/bin/bash
#@author: Francisco Jose Lozano Serrano

declare -a SCHEDULES=("RR" "SJF" "FCFS" "PRIO")
declare -i cpus 

set -e 	#Exit as soon as any line fails
#set -x  #Debugging

#Colors
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
RED=`tput setaf 1`
END_COLOR=`tput sgr0`

#Ask for the input task file to be simulated
echo "Please, enter the input task file to be simulated: "
read input_task
while [ ! -f $input_task ]; do
	echo "${RED}The specified file is not valid${END_COLOR}"
    echo "Please, enter the input task file to be simulated: "
	read input_task  
done

#Ask for the maximum number of CPUs to use during the simulation
echo "Please, enter the maximum number of CPUs to use during the simulation: "
read max_cpus
while [ $max_cpus -lt 1 -o $max_cpus -gt 8 ]; do
	echo "${RED}The specified number must be from 1 to 8${END_COLOR}"
    echo "Please, enter the maximum number of CPUs to use during the simulation: "
	read max_cpus  
done

if [ -d results ]; then
	rm -rf results
fi

mkdir results

#Copy the gantt plot tools to the folder
cp ../gantt-gplot/* results

for schedule in "${SCHEDULES[@]}"
do
	for(( cpus=1; cpus<=$max_cpus; cpus++))
	do
		echo $cpus
		./schedsim -i $input_task -s $schedule -n $cpus 
		for((i=0;$i<$cpus;i++))
		do
			mv CPU_$i.log results/$schedule'-cpus'_$cpus'-CPU_'$i'.log'
			##This is because the generate_gantt_chart program fails if the file isn't in the same folder
			cd results
			./generate_gantt_chart $schedule'-cpus'_$cpus'-CPU_'$i'.log'
			cd ..
		done
	done
done

mkdir results/logs results/diagrams

#Organize and clear the results folder
mv results/*.log results/logs/
mv results/*.eps results/diagrams/
rm -f results/* 2>/dev/null





