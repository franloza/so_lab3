#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>

int total_sum = 0;
/*Semaphore used as a mutex initializating it to 1*/
sem_t sem_mutex;

void * partial_sum(void * arg) {
  int j = 0;
  int ni=((int*)arg)[0];
  int nf=((int*)arg)[1];
  printf("ni: %i, nf: %i\n",ni,nf);

  for (j = ni; j <= nf; j++) {
    sem_wait(&sem_mutex);
    total_sum = total_sum + j;
    sem_post(&sem_mutex);
  }
  free(arg);
  pthread_exit(0);
}

int main(int argc, char* argv[]) {
  
  int upperB = 10000;
  int numThreads = 2;
  if (argc > 1 && atoi(argv[1]) > 0) {
    upperB = atoi(argv[1]);
    if (argc > 2 && atoi(argv[2]) > 0) {
      numThreads = atoi(argv[2]);
    }
  }

  int* num = 0;

  printf("Calculating the summation of numbers ranging between 1 and %i ", upperB);
  printf("using %i threads \n",numThreads);

  /*Initialization of the semaphore (not shared) to 1*/
  sem_init(&sem_mutex,0,1);

  /* Threads generator*/
  pthread_t* th = malloc(sizeof(pthread_t)*numThreads);
  int i;
  int sum = 1, increment = upperB/numThreads;
  for (i=0; i < numThreads-1;i++) {
    printf("Initiating thread number %i\n",i+1);
    num = malloc(sizeof(int)*2);
    num[0] = sum;num[1] = sum + increment - 1;
    pthread_create(&th[i],NULL,partial_sum,(void*)num);
    sum += increment;
  }
  /*The last thread receives the last part + remainder in case the upper limit
  is not divisible by the number of threads*/
  num = malloc(sizeof(int)*2);
  num[0] = sum;num[1] = upperB;
  printf("Initiating thread number %i\n",numThreads);
  pthread_create(&th[numThreads-1],NULL,partial_sum,(void*)num);
  
  printf("The main thread continues running\n");

  /* the main thread waits until all the threads have finished */

  for (i=0; i < numThreads;i++) {
    pthread_join(th[i], NULL);
  }
  
  //Calculate the expected result for testing
  int result = 0 ,j;
   for (j = 1; j <= upperB; j++) {
    result += j;   
  }

  num[0] = 0;num[1] = upperB;
  printf("total_sum=%d and the expected result is %i\n", total_sum,result);

  sem_destroy(&sem_mutex);
  free(th);
  return 0;
}
